all: get-deps-compile

get-deps:
	rebar get-deps

get-deps-compile:
	rebar get-deps compile

get-deps-compiledev: get-deps-compile

compile:
	rebar compile

compiledev: compile

clean:
	rebar clean

list-deps:
	rebar list-deps
list-templates:
	rebar list-templates
eunit:
	rebar skip_deps=true eunit
xref:
	rebar xref

cc: clean compile

ccx: clean compile xref eunit
